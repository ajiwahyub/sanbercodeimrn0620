class Animal {
    // Code class di sini
    constructor(animal) {
        this._aniname = animal;
        this.legs = 4;
        this.cold_blooded = false;
      }
      get name() {
        return this._aniname;
      }
}
 
var sheep = new Animal("shaun");
 
console.log(sheep.name) // "shaun"
console.log(sheep.legs) // 4
console.log(sheep.cold_blooded) // false

// Code class Ape dan class Frog di sini
class Frog extends Animal {
    constructor(animal) {
      super(animal)
      this.cold_blooded = true;
    }
    jump() {
      return console.log('hop hop');
    }
  }
  class Ape extends Animal {
    constructor(animal) {
      super(animal)
      this.legs = 2;
    }
    yell() {
      return console.log('Auooo');
    }
  }
var sungokong = new Ape("kera sakti")

 
var kodok = new Frog("buduk")


console.log(kodok.name) // "shaun"
console.log(kodok.legs) // 4
console.log(kodok.cold_blooded) // false
console.log(sungokong.name) // "shaun"
console.log(sungokong.legs) // 4
console.log(sungokong.cold_blooded) // false
sungokong.yell() // "Auooo"
kodok.jump() // "hop hop"  

class Clock {
    constructor({template}){
        this.template = template
        this.timer
    }
    render() {
        var date = new Date();
    
        var hours = date.getHours();
        if (hours < 10) hours = '0' + hours;
    
        var mins = date.getMinutes();
        if (mins < 10) mins = '0' + mins;
    
        var secs = date.getSeconds();
        if (secs < 10) secs = '0' + secs;
    
        var output = this.template
          .replace('h', hours)
          .replace('m', mins)
          .replace('s', secs);
    
        console.log(output);
    }
    stop() {
        clearInterval(timer);
      };
    start() {
        this.render();
        this.timer = setInterval(this.render.bind(this), 1000);
      };

}
var clock = new Clock({template: 'h:m:s'});
clock.start();  