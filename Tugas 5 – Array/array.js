// NO 1
function range(start, end)
{
    var arr = [0]
    arr[0] = start
    if (start < end){
        for (var i=1;i<=end-start;i++){
            arr[i]=start+i;
        }
        
    }
    else if (start >= end){
        for (var i=1;i<=start-end;i++){
            arr[i]=start-i;
        }
    }
    else if (start == null || end == null)
        arr = -1;
    return arr;

}
console.log(range(1, 10)) 
console.log(range(1))
console.log(range(11,18)) 
console.log(range(54, 50)) 
console.log(range()) 

//NO 2


function rangeWithStep(start1, end1, step)
{
    var arr1 = [0]
    arr1[0] = start1
    var temp = 0
    
    if (start1 < end1){
        
        while (start1 <= end1){
            arr1[temp]=start1;
            start1=start1+step;
            temp++;
        }
        
    }
    else if (start1 > end1){
        while (start1 >= end1){
            arr1[temp]=start1;
            start1=start1-step;
            temp++
        }
    }
   
    return arr1;

}
console.log(rangeWithStep(1, 10, 2)); // [1, 3, 5, 7, 9]
console.log(rangeWithStep(11, 23, 3)) ;// [11, 14, 17, 20, 23]
console.log(rangeWithStep(5, 2, 1)) ;// [5, 4, 3, 2]
console.log(rangeWithStep(29, 2, 4)) ;// [29, 25, 21, 17, 13, 9, 5] 
// NO 3
function sum(start2, end2, step1)
{
    var arr2 = [0]
    arr2[0] = start2
    var temp = 0
    if (step1 == null)
       { step1 = 1;}
        total = 0
    if (start2 < end2){
        while (start2 <= end2){
            arr2[temp]=start2;
            total = total + arr2[temp]
            start2=start2+step1;
            temp++;
        }
        
    }
    else if (start2 > end2){
        while (start2 >= end2){
            arr2[temp]=start2;
            total = total + arr2[temp]
            start2=start2-step1;
            temp++
        }
    }
   if (end2 == null)
        total = start2;
    if (start2 == null)
        total = 0;
    return total;

}
console.log(sum(1,10)); // 55
console.log(sum(5, 50, 2)); // 621
console.log(sum(15,10)); // 75
console.log(sum(20, 10, 2)); // 90
console.log(sum(1)) ;// 1
console.log(sum()); // 0

//No 4
function dataHandling(j){
    for (var j=0;j<3;j++){
        console.log("Nomor ID:\t "+input[j][0]);
        console.log("Nama Lengkap: \t "+input[j][2]);
        console.log("TTL:\t "+input[0][2]+input[j][3]);
        console.log("Hobi:\t "+input[j][4]);
        console.log("\n");
    }
}
var input = [
    ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
    ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
    ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
    ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
]
dataHandling();
console.log("\n");

//No 5
function balikKata(string){
    var akhir = string.length-1;
    var temp = "";
    while (akhir >= 0){
        temp = temp + string[akhir];
        akhir--;
    }
    return temp;
}
console.log(balikKata("Kasur Rusak"));// kasuR rusaK
console.log(balikKata("SanberCode"));// edoCrebnaS
console.log(balikKata("Haji Ijah"));// hajI ijaH
console.log(balikKata("racecar"));// racecar
console.log(balikKata("I am Sanbers")); // srebnaS ma I 