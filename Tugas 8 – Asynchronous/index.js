// di index.js
var readBooks = require('./callback.js')
 
var books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000}
] 
 
// Tulis code untuk memanggil function readBooks di sini
var waktu = 10000;

function index(i){
    if(i == books.length) {
        return 0;
    } else {
        readBooks(waktu, books[i], function(check) {
            waktu = check;
            index(i+1)
        });

    }
    
}
index(0);