/*
  A. Bandingkan Angka (10 poin)
    Buatlah sebuah function dengan nama bandingkan() yang menerima sebuah parameter berupa number 
    dan bilangan asli (positif). Jika salah satu atau kedua paramater merupakan bilangan negatif 
    maka function akan mereturn -1. Function tersebut membandingkan kedua parameter 
    dan mereturn angka yang lebih besar di antara keduanya. Jika kedua parameter sama besar 
    maka function akan mereturn nilai -1. 

  B. Balik String (10 poin)
    Diketahui sebuah function balikString yang menerima satu buah parameter berupa tipe data string. Function balikString akan mengembalikan sebuah string baru yang merupakan string kebalikan dari parameter yang diberikan. contoh: balikString("Javascript") akan me-return string "tpircsavaJ", balikString("satu") akan me-return string "utas", dst.

    NB: TIDAK DIPERBOLEHKAN menggunakan built-in function Javascript seperti .split(), .join(), .reverse() . 
    Hanya boleh gunakan looping. 

  C. Palindrome (10 poin)
    Buatlah sebuah function dengan nama palindrome() yang menerima sebuah parameter berupa String. 
    Function tersebut mengecek apakah string tersebut merupakan sebuah palindrome atau bukan. 
    Palindrome yaitu sebuah kata atau kalimat yang jika dibalik akan memberikan kata atau kalimat yang sama. 
    Function akan me-return tipe data boolean:  true jika string merupakan palindrome, dan false jika string bukan palindrome. 

  
    NB: TIDAK DIPERBOLEHKAN menggunakan built-in function Javascript seperti .split(), .join(), .reverse() . 
    Hanya boleh gunakan looping. 
  
    
*/

function bandingkan(num1, num2) {
  var a = 0
  if((num1 >= 0) &&  (num2 >= 0)){
    if(num1 > num2)
    {a = num1}
    if(num1 < num2 ) 
    {a = num2}
    else
    {a = -1}
    } 
  else {
    a = -1 
    } 
    return a;
 }
 console.log(bandingkan(10, 15)); // 15
console.log(bandingkan(12, 12)); // -1
console.log(bandingkan(-1, 10)); // -1 
console.log(bandingkan(112, 121));// 121
console.log(bandingkan(1)); // 1
console.log(bandingkan()); // -1
console.log(bandingkan("15", "18")) // 18
 

function balikString(string){
  var akhir = string.length-1;
  var temp = "";
  while (akhir >= 0){
      temp = temp + string[akhir];
      akhir--;
  }
  return temp;
}
console.log(balikString("abcde"));// edcba
console.log(balikString("rusak")); // kasur
console.log(balikString("racecar")); // racecar
console.log(balikString("haji")); // ijah

function palindrome(str) {
  var b = 0
  var c = str.length -1
  var d = ""
  while (c > b) 
    { 
        if (str[b] == str[c]) {
          d = "true"
                 }
        if (str[b] != str[c]) 
        { 
          d = "false"
        } 
        c--
        b++
    } 
      return d
}
console.log(palindrome("kasur rusak")); // true
console.log(palindrome("haji ijah"));// true
console.log(palindrome("nabasan")); // false
console.log(palindrome("nababan")); // true
console.log(palindrome("jakarta")); // false