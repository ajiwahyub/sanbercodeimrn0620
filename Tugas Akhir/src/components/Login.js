import React from 'react';
import { StyleSheet,Image ,Text, View, TextInput, TouchableOpacity  } from 'react-native';


export default function Login(props) {
  return (
    <View style={styles.container}>
       
      <Text style={styles.header}>Moviga</Text>
      <Text style={styles.header1}>your favorite movie information</Text>
      <Image  source={require("../../assets/logo.png")} style={{width:"80%",height:"30%",top:10,marginBottom:30}} resizeMode="contain" />
      <Text style={styles.header2}>Login</Text>
      <TextInput style={styles.input} placeholder="Email..." />
      <TextInput style={styles.input} placeholder="Password..." secureTextEntry />
      <TouchableOpacity style={styles.startBtn} onPress={()=>props.navigation.navigate("Home")}>
        <Text style={styles.startText}>LOGIN</Text>
      </TouchableOpacity>

    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  header:{
    fontWeight:"bold",
    fontSize:36,
    color:"#18191F",
    marginBottom:20,
  
    height: 50,
    top: 0,

  },
  header1:{
    fontWeight:"normal",
    fontSize:18,
    color:"#18191F",
    marginBottom:20,
    width : "50%",
    textAlign : "center",
    height: 50,
    top: 10,

  },
  header2:{
    fontWeight:"bold",
    fontSize:23,
    color:"#18191F",
    marginBottom:10,
    width : "50%",
    textAlign : "center",
    height: 40,
    top: 10,

  },
  input:{
    borderWidth:1,
    borderColor:"#18191F",
    borderRadius:50,
    width:"80%",
    height:40,
    marginBottom:20,
    padding:10,
    top : 10
  },
  startBtn:{
    backgroundColor:"#18191F",
    borderRadius:50,
    padding:10,
    width:"50%",
    alignItems:"center",
    marginTop:20
  },
  startText:{
    color:"white"
  }
});
