import * as React from 'react';
import { createStackNavigator } from '@react-navigation/stack'
import { Text, View } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createMaterialBottomTabNavigator } from '@react-navigation/material-bottom-tabs';
import { MaterialCommunityIcons } from '@expo/vector-icons';
import Movie from "./Movie"
import Aboutus from "./Aboutus"
function Feed() {
  return (
    <Movie/>
  );
}

function AboutUs() {
  return (

  <Aboutus/>

  );
}

const Tab = createMaterialBottomTabNavigator();

function MyTabs() {
  return (
    <Tab.Navigator
      initialRouteName="Feed"
      activeColor="#e91e63"
      labelStyle={{ fontSize: 12 }}
      style={{ backgroundColor: 'tomato' }}
    >
      <Tab.Screen
        name="Feed"
        component={Feed}
        options={{
          tabBarLabel: 'Home',
          tabBarIcon: ({ color }) => (
            <MaterialCommunityIcons name="home" color={color} size={26} />
          ),
        }}
      />
      
      <Tab.Screen
        name="About Us"
        component={AboutUs}
        options={{
          tabBarLabel: 'About Us',
          tabBarIcon: ({ color }) => (
            <MaterialCommunityIcons name="account" color={color} size={26} />
          ),
        }}
      />
    </Tab.Navigator>
  );
}

export default function Home(props) {
  return (
    <NavigationContainer>
      <MyTabs />
    </NavigationContainer>
  );
}