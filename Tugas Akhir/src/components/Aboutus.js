import React from 'react';
import { StyleSheet, Text, View, Image, TouchableOpacity  } from 'react-native';


export default function Aboutus(props) {
  return (
    <View style={styles.container}>
      
      <Text style={styles.header}>About Us</Text>
      <Image  source={require("../../assets/logo.png")} style={{width:"80%",height:"30%"}} resizeMode="contain" />
      <Text style={styles.description}>Founder</Text>
      <Text style={styles.description}>Aji Wahyubuwono</Text>
      <Text style={styles.description}>Co-Founder</Text>
      <Text style={styles.description}>Anggara Catra P</Text>
      
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  header:{
    fontWeight:"bold",
    fontSize:24,
    color:"#375177",
    marginBottom:10
  },
  description:{
    fontSize:12,
    color:"gray",
    padding:5
  },
 
});
