import {createAppContainer} from 'react-navigation';
import {createStackNavigator} from 'react-navigation-stack';

import Login from "./src/components/Login"
import Home from "./src/components/Home"

const MainNavigator = createStackNavigator({
  Login: {screen: Login},
  Home: {screen: Home},
  
},
{
  headerMode: 'none',
  navigationOptions: {
      headerVisible: false,
  }
});

const App = createAppContainer(MainNavigator);

export default App;
